import { Router } from 'express';
import * as authService from '../services/authService';
import * as userService from '../services/userService';
import authenticationMiddleware from '../middlewares/authenticationMiddleware';
import registrationMiddleware from '../middlewares/registrationMiddleware';
import jwtMiddleware from '../middlewares/jwtMiddleware';

const router = Router();

// user added to the request (req.user) in a strategy, see passport config
router
  .get('/user', jwtMiddleware, (req, res, next) => userService.getUserById(req.user.id)
    .then(data => res.send(data))
    .catch(next))
  .get('/token/:token', (req, res, next) => userService.getByResetToken(req.params.token)
    .then(data => res.send(data))
    .catch(next))
  .post('/login', authenticationMiddleware, (req, res, next) => authService.login(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/register', registrationMiddleware, (req, res, next) => authService.register(req.user)
    .then(data => res.send(data))
    .catch(next))
  .post('/gen_reset_password', (req, res, next) => userService.generatePasswordResetToken(req.body.email)
    .then(data => res.send(data))
    .catch(next))
  .post('/reset_password/:token', (req, res, next) => userService.resetPassword(
    req.params.token,
    req.body.password
  )
    .then(data => res.send(data))
    .catch(next))
  .put('/update', jwtMiddleware, (req, res, next) => userService.updateById(req.user.id, req.body)
    .then(data => res.send(data))
    .catch(next));

export default router;
