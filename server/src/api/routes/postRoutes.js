import { Router } from 'express';
import * as postService from '../services/postService';

const router = Router();

router
  .get('/', (req, res, next) => postService.getPosts(req.query)
    .then(posts => res.send(posts))
    .catch(next))
  .get('/:id', (req, res, next) => postService.getPostById(req.params.id)
    .then(post => res.send(post))
    .catch(next))
  .post('/', (req, res, next) => postService.create(req.user.id, req.body)
    .then(post => {
      req.io.emit('new_post', post); // notify all users that a new post was created
      return res.send(post);
    })
    .catch(next))
  .post('/share', (req, res, next) => postService.sharePostByEmail(
    req.body.email,
    req.user.username,
    req.body.url
  )
    .then(data => res.send(data))
    .catch(next))
  .put('/react', (req, res, next) => postService.setReaction(req.user.id, req.body)
    .then(reaction => {
      if (reaction.post && (reaction.post.userId !== req.user.id)) {
        // notify a user if someone (not himself) liked his post
        const reactionWord = reaction.isLike ? 'liked!' : 'disliked.';
        req.io.to(reaction.post.userId).emit('like', `Your post was ${reactionWord}`);
      }
      return res.send(reaction);
    })
    .catch(next))
  .put('/:id', (req, res, next) => postService.update(req.params.id, req.body)
    .then(post => {
      req.io.emit('update_post', req.params.id, post);
      return res.send(post);
    })
    .catch(next))
  .delete('/:id', (req, res, next) => postService.deletePost(req.params.id)
    .then(post => {
      req.io.emit('delete_post', req.params.id);
      return res.send(post);
    })
    .catch(next));

export default router;
