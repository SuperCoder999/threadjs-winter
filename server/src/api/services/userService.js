import env from '../../env';
import userRepository from '../../data/repositories/userRepository';
import { encryptSync } from '../../helpers/cryptoHelper';
import { generateRandomString } from '../../helpers/randomString';
import { resetTokenExpireMin } from '../../config/resetPasswordConfig';
import { sendTemplateEmail } from '../../helpers/emailHelper';
import { resetPasswordTemplate } from '../../config/emailConfig';

export const getUserById = async userId => {
  const { id, username, email, imageId, image, status } = await userRepository.getUserById(userId);
  return { id, username, email, imageId, image, status };
};

export const updateById = async (userId, data) => {
  const { id, username, email } = await userRepository.updateById(userId, data);
  return { id, username, email };
};

export const getByResetToken = async token => {
  const user = await userRepository.getByResetToken(token);

  if (!user) {
    // eslint-disable-next-line no-throw-literal
    throw { status: 400, message: 'Invalid or expired reset password token' };
  }

  const { id, username, email, resetPasswordExpire } = user;

  if (!resetPasswordExpire || resetPasswordExpire < new Date()) {
    // eslint-disable-next-line no-throw-literal
    throw { status: 400, message: 'Invalid or expired reset password token' };
  }

  return { id, username, email };
};

export const generatePasswordResetToken = async userEmail => {
  const user = await userRepository.getByEmail(userEmail);

  if (!user) {
    return {};
  }

  const userId = user.id;
  const expireDate = new Date();
  expireDate.setMinutes(expireDate.getMinutes() + resetTokenExpireMin);

  const token = generateRandomString();

  const { id, username, email } = await userRepository.updateById(userId, {
    resetPasswordToken: token,
    resetPasswordExpire: expireDate
  });

  sendTemplateEmail({
    to: email,
    template: resetPasswordTemplate,
    values: [username, `${env.app.frontendUrl}/reset_password/${token}`]
  });

  return { id, username, email };
};

export const resetPassword = async (token, newPassword) => {
  const user = await userRepository.getByResetToken(token);

  if (!user) {
    // eslint-disable-next-line no-throw-literal
    throw { status: 400, message: 'Invalid or expired reset password token' };
  }

  const { id, username, email, resetPasswordToken, resetPasswordExpire } = user;
  const tokenEqual = resetPasswordToken === token;
  const expired = !resetPasswordExpire || resetPasswordExpire < new Date();

  if (!resetPasswordToken || !tokenEqual || expired) {
    // eslint-disable-next-line no-throw-literal
    throw { status: 400, message: 'Invalid or expired reset password token' };
  }

  await userRepository.updateById(id, {
    resetPasswordToken: null,
    resetPasswordExpire: null,
    password: encryptSync(newPassword)
  });

  return { id, username, email };
};
