import commentRepository from '../../data/repositories/commentRepository';
import commentReactionRepository from '../../data/repositories/commentReactionRepository';

export const create = (userId, comment) => commentRepository.create({
  ...comment,
  userId
});

export const getCommentById = id => commentRepository.getCommentById(id);

export const update = (commentId, data) => commentRepository.updateById(commentId, data);

export const deleteComment = commentId => commentRepository.deleteCommentById(commentId);

export const setReaction = async (userId, { commentId, isLike = true }) => {
  // define the callback for future use as a promise
  let updated = false;

  const updateOrDelete = react => {
    if (react.isLike === isLike) {
      return commentReactionRepository.deleteById(react.id);
    }

    updated = true;
    return commentReactionRepository.updateById(react.id, { isLike });
  };

  const reaction = await commentReactionRepository.getCommentReaction(userId, commentId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await commentReactionRepository.create({ userId, commentId, isLike });

  // the result is an integer when an entity is deleted
  if (Number.isInteger(result)) {
    return {};
  }

  const newReaction = await commentReactionRepository.getCommentReaction(userId, commentId);
  return { ...newReaction.dataValues, updated };
};
