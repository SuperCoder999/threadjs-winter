import env from '../../env';
import postRepository from '../../data/repositories/postRepository';
import postReactionRepository from '../../data/repositories/postReactionRepository';
import userRepository from '../../data/repositories/userRepository';
import { sharePostTemplate, likedPostTemplate } from '../../config/emailConfig';
import { sendTemplateEmail } from '../../helpers/emailHelper';

export const getPosts = filter => postRepository.getPosts(filter);

export const getPostById = id => postRepository.getPostById(id);

export const create = (userId, post) => postRepository.create({
  ...post,
  userId
});

export const update = (postId, data) => postRepository.updateById(postId, data);

export const deletePost = postId => postRepository.deletePostById(postId);

export const setReaction = async (userId, { postId, isLike = true }) => {
  // define the callback for future use as a promise
  let updated = false;

  const updateOrDelete = react => {
    if (react.isLike === isLike) {
      return postReactionRepository.deleteById(react.id);
    }

    updated = true;
    return postReactionRepository.updateById(react.id, { isLike });
  };

  const reaction = await postReactionRepository.getPostReaction(userId, postId);

  const result = reaction
    ? await updateOrDelete(reaction)
    : await postReactionRepository.create({ userId, postId, isLike });

  // the result is an integer when an entity is deleted
  if (Number.isInteger(result)) {
    return {};
  }

  const newReaction = await postReactionRepository.getPostReaction(userId, postId);

  if (newReaction.isLike) {
    const { user: { username, email, id } } = await postRepository.getPostById(postId);
    const { username: likerUsername, id: likerId } = await userRepository.getById(userId);

    if (id !== likerId) {
      sendTemplateEmail({
        to: email,
        template: likedPostTemplate,
        values: [
          username,
          likerUsername,
          `${env.app.frontendUrl}/share/${postId}`
        ]
      });
    }
  }

  return { ...newReaction.dataValues, updated };
};

export const sharePostByEmail = async (to, username, shareUrl) => {
  sendTemplateEmail({
    to,
    template: sharePostTemplate,
    values: [username, shareUrl]
  });

  return { success: true };
};
