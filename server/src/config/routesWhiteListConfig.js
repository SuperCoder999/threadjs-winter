export default [
  /\/auth\/login/,
  /\/auth\/register/,
  /\/auth\/reset_password/,
  /\/auth\/gen_reset_password/,
  /\/auth\/token\/[a-zA-Z0-9]+/
];
