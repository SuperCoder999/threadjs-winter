import env from '../env';
import { resetTokenExpireMin } from './resetPasswordConfig';

export const { gmailUser, gmailPassword, gmailSMTP } = env.mail;

export const resetPasswordTemplate = {
  subject: 'Password reset',
  message: `
    Hello, {0}.
    <br />
    You have requested password reset
    <br />
    Here is your reset password link: <a href="{1}">{1}</a>. It'll expire in ${resetTokenExpireMin} minutes.
    <br />
    <br />
    If you haven't requested password reset, ignore this message.
  `
};

export const sharePostTemplate = {
  subject: 'Post share',
  message: `
    Hello.
    <br />
    <br />
    {0} shared a post with you.
    <br />
    <br />
    Have a look: {1}
  `
};

export const likedPostTemplate = {
  subject: 'Your post was liked',
  message: `
    Hello, {0}.
    <br />
    <br />
    {1} liked your post!
    <br />
    <br />
    Have a look: {2}
  `
};
