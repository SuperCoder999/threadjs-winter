import { Op } from 'sequelize';
import sequelize from '../db/connection';

import {
  PostModel,
  CommentModel,
  UserModel,
  ImageModel,
  PostReactionModel,
  CommentReactionModel
} from '../models/index';

import BaseRepository from './baseRepository';

const likeCase = bool => `
(SELECT COUNT(*)
FROM "postReactions" as "react"
WHERE "post"."id" = "react"."postId" AND "react"."isLike" = ${bool})
`;

const commentLikeCase = bool => `
(SELECT COUNT(*)
FROM "commentReactions" AS "react"
WHERE "react"."commentId" = "comments"."id" AND "react"."isLike" = ${bool})
`;

const commentCount = `
(SELECT COUNT(*)
FROM "comments" AS "comment"
WHERE "comment"."postId" = "post"."id" AND "comment"."deletedAt" IS NULL)
`;

class PostRepository extends BaseRepository {
  async getPosts(filter) {
    const {
      from: offset,
      count: limit,
      showLikedByUser,
      excludeUserId,
      userId
    } = filter;

    const where = {
      deletedAt: null
    };

    if (userId) {
      Object.assign(where, { userId });
    }

    if (excludeUserId) {
      Object.assign(where, {
        userId: {
          [Op.not]: excludeUserId
        }
      });
    }

    if (showLikedByUser) {
      Object.assign(where, {
        '$postReactions.userId$': showLikedByUser,
        '$postReactions.isLike$': true
      });
    }

    return this.model.findAll({
      where,
      attributes: {
        include: [
          [sequelize.literal(commentCount), 'commentCount'],
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [
        {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        {
          model: UserModel,
          attributes: ['id', 'username', 'email'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: PostReactionModel,
          attributes: ['id', 'isLike'],
          duplicating: false,
          include: {
            model: UserModel,
            attributes: ['id', 'username']
          }
        }
      ],
      group: [
        'post.id',
        'image.id',
        'user.id',
        'user->image.id',
        'postReactions.id',
        'postReactions->user.id'
      ],
      order: [['createdAt', 'DESC']],
      offset,
      limit
    });
  }

  getPostById(id) {
    return this.model.findOne({
      group: [
        'post.id',
        'comments.id',
        'comments->user.id',
        'comments->user->image.id',
        'comments->commentReactions.id',
        'comments->commentReactions->user.id',
        'user.id',
        'user->image.id',
        'image.id',
        'postReactions.id',
        'postReactions->user.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(commentCount), 'commentCount'],
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [
        {
          model: CommentModel,
          attributes: {
            include: [
              [sequelize.literal(commentLikeCase(true)), 'likeCount'],
              [sequelize.literal(commentLikeCase(false)), 'dislikeCount']
            ]
          },
          include: [
            {
              model: UserModel,
              attributes: ['id', 'username', 'status'],
              include: {
                model: ImageModel,
                attributes: ['id', 'link']
              }
            },
            {
              model: CommentReactionModel,
              attributes: ['id', 'isLike'],
              include: {
                model: UserModel,
                attributes: ['id', 'username']
              }
            }
          ]
        },
        {
          model: UserModel,
          attributes: ['id', 'username', 'email'],
          include: {
            model: ImageModel,
            attributes: ['id', 'link']
          }
        },
        {
          model: ImageModel,
          attributes: ['id', 'link']
        },
        {
          model: PostReactionModel,
          attributes: ['id', 'isLike'],
          duplicating: false,
          include: {
            model: UserModel,
            attributes: ['id', 'username']
          }
        }
      ]
    });
  }

  deletePostById(id) {
    return this.updateById(id, { deletedAt: new Date() });
  }
}

export default new PostRepository(PostModel);
