import sequelize from '../db/connection';
import { CommentModel, UserModel, ImageModel } from '../models/index';
import BaseRepository from './baseRepository';

const likeCase = bool => `
(SELECT COUNT(*)
FROM "commentReactions" AS "react"
WHERE "react"."commentId" = "comment"."id" AND "react"."isLike" = ${bool})
`;

class CommentRepository extends BaseRepository {
  getCommentById(id) {
    return this.model.findOne({
      group: [
        'comment.id',
        'user.id',
        'user->image.id'
      ],
      where: { id },
      attributes: {
        include: [
          [sequelize.literal(likeCase(true)), 'likeCount'],
          [sequelize.literal(likeCase(false)), 'dislikeCount']
        ]
      },
      include: [{
        model: UserModel,
        attributes: ['id', 'username'],
        include: {
          model: ImageModel,
          attributes: ['id', 'link']
        }
      }]
    });
  }

  deleteCommentById(id) {
    return this.updateById(id, { deletedAt: new Date() });
  }
}

export default new CommentRepository(CommentModel);
