module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.addColumn(
        'users',
        'resetPasswordToken',
        { type: Sequelize.STRING },
        { transaction }
      ),
      queryInterface.addColumn(
        'users',
        'resetPasswordExpire',
        { type: Sequelize.DATE },
        { transaction }
      )
    ])),
  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => Promise.all([
      queryInterface.removeColumn('users', 'resetPasswordToken', { transaction }),
      queryInterface.removeColumn('users', 'resetPasswordExpire', { transaction })
    ]))
};
