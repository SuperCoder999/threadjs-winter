module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => queryInterface.addColumn(
      'users',
      'status',
      { type: Sequelize.STRING },
      { transaction }
    )),
  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => queryInterface.removeColumn('users', 'status', { transaction }))
};
