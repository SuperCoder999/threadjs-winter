module.exports = {
  up: (queryInterface, Sequelize) => queryInterface.sequelize
    .transaction(transaction => queryInterface.addColumn(
      'posts',
      'deletedAt',
      { type: Sequelize.DATE },
      { transaction }
    )),
  down: queryInterface => queryInterface.sequelize
    .transaction(transaction => queryInterface.removeColumn('posts', 'deletedAt', { transaction }))
};
