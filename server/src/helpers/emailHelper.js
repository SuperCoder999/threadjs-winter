import nodemailer from 'nodemailer';
import { gmailUser, gmailPassword, gmailSMTP } from '../config/emailConfig';
import { formatString } from './stringFormatHelper';

const configure = () => {
  const transporter = nodemailer.createTransport({
    host: gmailSMTP,
    port: 465,
    secure: true,
    auth: {
      user: gmailUser,
      pass: gmailPassword
    }
  });

  return transporter;
};

export const sendEmail = ({ to, subject, message }) => {
  const transporter = configure();

  return transporter.sendMail({
    from: `ThreadJS <${gmailUser}>`,
    to,
    subject,
    html: message
  });
};

export const sendTemplateEmail = ({ to, template, subjectValues = [], values = [] }) => {
  const { message, subject } = template;

  return sendEmail({
    to,
    subject: formatString(subject, subjectValues),
    message: formatString(message, values)
  });
};
