const defaultAlphabet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890';

export const generateRandomString = (length = 64, alphabet = defaultAlphabet) => {
  let string = '';

  for (let i = 0; i < length; i++) {
    const index = Math.floor(Math.random() * alphabet.length);
    const char = alphabet[index];
    string += char;
  }

  return string;
};
