export const formatString = (string, formatValues) => {
  let newString = string;

  formatValues.forEach((value, index) => {
    newString = newString.replaceAll(`{${index}}`, value);
  });

  return newString;
};
