import React, { useEffect, useState } from 'react';
import { useHistory, useRouteMatch } from 'react-router-dom';
import { NotificationManager } from 'react-notifications';
import { Grid, Header, Form, Button } from 'semantic-ui-react';
import * as service from '../../services/authService';
import Spinner from '../../components/Spinner';

const ResetPasswordPage = () => {
  const { params: { token } } = useRouteMatch();
  const { push } = useHistory();
  const [loading, setLoading] = useState(true);
  const [canShowContent, setCanShowContent] = useState(false);
  const [password, setPasswordText] = useState('');
  const [passwordValid, setPasswordValid] = useState(true);

  const setPassword = value => {
    setPasswordText(value);
    setPasswordValid(true);
  };

  useEffect(() => {
    async function checkUser() {
      try {
        await service.getByResetToken(token);
        setCanShowContent(true);
      } catch (err) {
        NotificationManager.error(err.message || err, 'Error');
        setTimeout(push, 2000, '/');
      } finally {
        setLoading(false);
      }
    }

    checkUser();
  }, [token, push]);

  const resetPassword = async () => {
    if (!password || !passwordValid) {
      return;
    }

    setLoading(true);

    try {
      await service.resetPassword(token, password);
      push('/');
    } catch (err) {
      NotificationManager.error(err.message, 'Error');
    } finally {
      setLoading(false);
    }
  };

  return (
    <>
      {loading ? <Spinner /> : null}
      {canShowContent ? (
        <Grid textAlign="center" verticalAlign="middle" className="fill">
          <Grid.Column style={{ maxWidth: 450 }}>
            <Header as="h2" color="teal" textAlign="center">
              Reset password
            </Header>
            <Form>
              <Form.Input
                value={password}
                type="password"
                icon="lock"
                iconPosition="left"
                placeholder="Enter new password"
                onChange={(event, data) => setPassword(data.value)}
                onBlur={() => setPasswordValid(password.length > 5)}
              />
              <Button type="submit" color="yellow" onClick={resetPassword}>Reset password</Button>
            </Form>
          </Grid.Column>
        </Grid>
      ) : null}
    </>
  );
};

export default ResetPasswordPage;
