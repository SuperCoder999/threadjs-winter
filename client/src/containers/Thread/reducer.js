import {
  SET_ALL_POSTS,
  LOAD_MORE_POSTS,
  ADD_POST,
  UPDATE_POST,
  SET_EXPANDED_POST,
  DELETE_POST
} from './actionTypes';

export default (state = {}, action) => {
  switch (action.type) {
    case SET_ALL_POSTS:
      return {
        ...state,
        posts: action.posts,
        hasMorePosts: Boolean(action.posts.length)
      };
    case LOAD_MORE_POSTS:
      return {
        ...state,
        posts: [...(state.posts || []), ...action.posts],
        hasMorePosts: Boolean(action.posts.length)
      };
    case ADD_POST:
      return {
        ...state,
        posts: [action.post, ...state.posts]
      };
    case UPDATE_POST: {
      const { postId, post } = action;
      const index = state.posts.findIndex(suspectedPost => suspectedPost.id === postId);

      if (index < 0) {
        return state;
      }

      const postToUpdate = state.posts[index];

      const newPost = {
        ...postToUpdate,
        ...post
      };

      const newPosts = [...state.posts];
      newPosts[index] = newPost;

      return {
        ...state,
        posts: newPosts
      };
    }
    case DELETE_POST: {
      const { postId } = action;
      const index = state.posts.findIndex(suspectedPost => suspectedPost.id === postId);

      if (index < 0) {
        return state;
      }

      const newPosts = [...state.posts];
      newPosts.splice(index, 1);

      return {
        ...state,
        posts: newPosts
      };
    }
    case SET_EXPANDED_POST:
      return {
        ...state,
        expandedPost: action.post
      };
    default:
      return state;
  }
};
