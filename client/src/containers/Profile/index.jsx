import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { getUserImgLink } from 'src/helpers/imageHelper';

import {
  Grid,
  Image,
  Input,
  Button,
  Dimmer,
  Icon
} from 'semantic-ui-react';

import MaskedFileInput from '../../components/MaskedFileInput';
import ImageCropModal from '../../components/ImageCropModal';
import * as imageService from '../../services/imageService';
import { updateProfile } from './actions';

import styles from './styles.module.scss';

const Profile = ({ user, updateProfile: update }) => {
  const [uploadedAvatar, setUploadedAvatar] = useState(null);
  const [imageDimmerActive, setImageDimmerActive] = useState(false);
  const [avatar, setAvatar] = useState(user.image);
  const [loading, setLoading] = useState(false);
  const [username, setUsername] = useState(user.username);
  const [status, setStatus] = useState(user.status);

  const btnDisabled = username === user.username
    && getUserImgLink(avatar) === getUserImgLink(user.image)
    && status === user.status;

  const onImageSubmit = image => {
    setUploadedAvatar(null);
    setLoading(true);

    imageService.uploadImage(image).then(result => {
      setLoading(false);
      setAvatar(result);
    });
  };

  const submit = () => {
    setLoading(true);

    update({
      username,
      status,
      imageId: avatar.id
    }).then(() => setLoading(false));
  };

  return (
    <Grid container textAlign="center" style={{ paddingTop: 30 }}>
      <Grid.Column>
        <MaskedFileInput onUpload={setUploadedAvatar}>
          <Dimmer.Dimmable
            blurring
            dimmed={imageDimmerActive}
            className={styles.imageWrapper}
            onMouseEnter={() => setImageDimmerActive(true)}
            onMouseLeave={() => setImageDimmerActive(false)}
          >
            <Image centered src={getUserImgLink(user.image)} size="medium" circular className={styles.image} />
            <Dimmer active={imageDimmerActive}>
              <Icon name="camera" size="huge" />
              <br />
              <br />
              Click to change profile photo
            </Dimmer>
          </Dimmer.Dimmable>
        </MaskedFileInput>
        {uploadedAvatar ? (
          <ImageCropModal
            image={uploadedAvatar}
            header="Edit your avatar"
            text="Avatar must be of size 200x200"
            onFinish={onImageSubmit}
          />
        ) : null}
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Username"
          type="text"
          value={username}
          onChange={(event, data) => setUsername(data.value)}
        />
        <br />
        <br />
        <Input
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          disabled
          value={user.email}
        />
        <br />
        <br />
        <Input
          icon="user"
          iconPosition="left"
          placeholder="Status"
          type="text"
          value={status}
          onChange={(event, data) => setStatus(data.value)}
        />
        <br />
        <br />
        <Button
          color="teal"
          disabled={btnDisabled}
          loading={loading}
          onClick={submit}
        >
          Save profile information
        </Button>
      </Grid.Column>
    </Grid>
  );
};

Profile.propTypes = {
  user: PropTypes.objectOf(PropTypes.any),
  updateProfile: PropTypes.func.isRequired
};

Profile.defaultProps = {
  user: {}
};

const mapStateToProps = rootState => ({
  user: rootState.profile.user
});

const actions = { updateProfile };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Profile);
