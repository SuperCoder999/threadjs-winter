import React, { useRef, useCallback } from 'react';
import PropTypes from 'prop-types';

const MaskedFileInput = ({ children, onUpload, accept }) => {
  const inputRef = useRef();

  const onChange = event => {
    const file = event.target.files[0];

    if (file) {
      onUpload(file);
    }
  };

  const setRef = useCallback(ref => {
    inputRef.current = ref;
  }, []);

  return (
    <div>
      <div onClick={() => inputRef.current.click()}>{children}</div>
      <input
        type="file"
        accept={accept}
        onChange={onChange}
        style={{ display: 'none' }}
        ref={setRef}
      />
    </div>
  );
};

MaskedFileInput.defaultProps = {
  accept: 'image/*'
};

MaskedFileInput.propTypes = {
  onUpload: PropTypes.func.isRequired,
  accept: PropTypes.string,
  children: PropTypes.objectOf(PropTypes.any).isRequired
};

export default MaskedFileInput;
