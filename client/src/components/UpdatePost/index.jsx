import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Image, Icon, Button } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';

import styles from './styles.module.scss';

const UpdatePost = ({ shown, postId, defaultBody, updatePost, defaultImage, uploadImage, onSubmit }) => {
  const [body, setBody] = useState(defaultBody);
  const [image, setImage] = useState(defaultImage);
  const [isUploading, setIsUploading] = useState(false);
  const display = shown ? 'block' : 'none';

  const cancel = () => {
    setBody(defaultBody);
    setImage(defaultImage);
    onSubmit();
  };

  const handleAddPost = async () => {
    if (!body) {
      return;
    }

    await updatePost(postId, { imageId: image?.imageId, body });
    onSubmit();
  };

  const handleUploadFile = async ({ target }) => {
    setIsUploading(true);

    try {
      const { id: imageId, link: imageLink } = await uploadImage(target.files[0]);
      setImage({ imageId, imageLink });
    } catch (err) {
      NotificationManager.error(err.message, 'Error');
    } finally {
      setIsUploading(false);
    }
  };

  return (
    <Form onSubmit={handleAddPost} style={{ display }} className={styles.form}>
      <Form.TextArea
        name="body"
        value={body}
        placeholder="What is the news?"
        onChange={ev => setBody(ev.target.value)}
      />
      {image?.imageLink && (
        <div className={styles.imageWrapper}>
          <Image className={styles.image} src={image?.imageLink} alt="post" />
        </div>
      )}
      <Button color="teal" icon labelPosition="left" as="label" loading={isUploading} disabled={isUploading}>
        <Icon name="image" />
        Attach image
        <input name="image" type="file" onChange={handleUploadFile} hidden />
      </Button>
      <Button.Group floated="right">
        <Button color="blue" type="submit">Update</Button>
        <Button onClick={cancel} type="button">Cancel</Button>
      </Button.Group>
    </Form>
  );
};

UpdatePost.defaultProps = {
  defaultImage: undefined
};

UpdatePost.propTypes = {
  shown: PropTypes.bool.isRequired,
  postId: PropTypes.string.isRequired,
  defaultBody: PropTypes.string.isRequired,
  defaultImage: PropTypes.objectOf(PropTypes.any),
  updatePost: PropTypes.func.isRequired,
  uploadImage: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default UpdatePost;
