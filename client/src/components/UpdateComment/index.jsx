import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Form, Button } from 'semantic-ui-react';

import styles from './styles.module.scss';

const UpdateComment = ({ shown, defaultBody, updateComment, onSubmit }) => {
  const [body, setBody] = useState(defaultBody);
  const display = shown ? 'block' : 'none';

  const cancel = () => {
    setBody(defaultBody);
    onSubmit();
  };

  const handleAddPost = async () => {
    if (!body) {
      return;
    }

    await updateComment({ body });
    onSubmit();
  };

  return (
    <Form onSubmit={handleAddPost} style={{ display }} className={styles.form}>
      <Form.TextArea
        name="body"
        value={body}
        placeholder="Edit your comment"
        onChange={ev => setBody(ev.target.value)}
      />
      <Button.Group floated="right">
        <Button color="blue" type="submit">Update</Button>
        <Button onClick={cancel} type="button">Cancel</Button>
      </Button.Group>
    </Form>
  );
};

UpdateComment.propTypes = {
  shown: PropTypes.bool.isRequired,
  defaultBody: PropTypes.string.isRequired,
  updateComment: PropTypes.func.isRequired,
  onSubmit: PropTypes.func.isRequired
};

export default UpdateComment;
