import React, { useState, useRef } from 'react';
import PropTypes from 'prop-types';
import { Modal, Input, Icon, Button } from 'semantic-ui-react';
import validator from 'validator';
import * as postService from '../../services/postService';

import styles from './styles.module.scss';

const SharedPostLink = ({ postId, close }) => {
  const [copied, setCopied] = useState(false);
  const [shareByEmailOpened, setShareByEmailOpened] = useState(false);
  const [shareEmail, setShareEmailText] = useState('');
  const [shareEmailValid, setShareEmailValid] = useState(true);
  const shareLink = `${window.location.origin}/share/${postId}`;

  const setShareEmail = value => {
    setShareEmailValid(validator.isEmail(value));
    setShareEmailText(value);
  };

  const resetEmailState = () => {
    setShareEmailText('');
    setShareEmailValid(true);
    setShareByEmailOpened(false);
  };

  const sharePostByEmail = () => {
    if (!shareEmail || !shareEmailValid) {
      return;
    }

    postService.sharePost(shareEmail, shareLink);
    resetEmailState();
  };

  let input = useRef();

  const copyToClipboard = e => {
    input.select();
    document.execCommand('copy');
    e.target.focus();
    setCopied(true);
  };

  return (
    <Modal open onClose={close}>
      <Modal.Header className={styles.header}>
        <span>Share Post</span>
        {copied && (
          <span>
            <Icon color="green" name="copy" />
            Copied
          </span>
        )}
      </Modal.Header>
      <Modal.Content>
        <Input
          fluid
          action={{
            color: 'teal',
            labelPosition: 'right',
            icon: 'copy',
            content: 'Copy',
            onClick: copyToClipboard
          }}
          value={shareLink}
          ref={ref => { input = ref; }}
        />
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => setShareByEmailOpened(true)}>Share post by email</Button>
      </Modal.Actions>
      <Modal open={shareByEmailOpened}>
        <Modal.Header>
          Share post by email
        </Modal.Header>
        <Modal.Content>
          <Input
            fluid
            icon="at"
            iconPosition="left"
            placeholder="Enter share email"
            error={!shareEmailValid}
            value={shareEmail}
            onChange={(event, data) => setShareEmail(data.value)}
          />
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={resetEmailState}>Cancel</Button>
          <Button primary onClick={sharePostByEmail}>Share!</Button>
        </Modal.Actions>
      </Modal>
    </Modal>
  );
};

SharedPostLink.propTypes = {
  postId: PropTypes.string.isRequired,
  close: PropTypes.func.isRequired
};

export default SharedPostLink;
