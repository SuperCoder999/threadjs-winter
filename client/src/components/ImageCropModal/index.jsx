import React, { useCallback, useEffect, useRef, useState } from 'react';
import PropTypes from 'prop-types';
import ReactCrop from 'react-image-crop';
import { Modal, Button } from 'semantic-ui-react';
import Spinner from '../Spinner';

import styles from './styles.module.scss';

const ImageCropModal = ({ image, onFinish, onCancel, header, text }) => {
  const [cropImg, setCropImg] = useState();
  const [currCrop, setCurrCrop] = useState({ unit: 'px', aspect: 1 / 1, width: 200, heigth: 200, x: 0, y: 0 });
  const [crop, setCrop] = useState();

  const imgRef = useRef();
  const canvasRef = useRef();

  const imageLoad = useCallback(ref => {
    imgRef.current = ref;
  }, []);

  const onCropChange = newCrop => {
    if (newCrop.width <= 200) {
      setCurrCrop(newCrop);
    }
  };

  const exportImage = () => {
    const canvas = canvasRef.current;

    if (!canvas) {
      return;
    }

    canvas.toBlob(blob => onFinish(new File([blob], 'untitled', { type: 'image/png' })), 'image/png', 1);
  };

  useEffect(() => {
    const reader = new FileReader();
    reader.addEventListener('load', () => setCropImg(reader.result));
    reader.readAsDataURL(new Blob([image]));
  }, [image]);

  useEffect(() => {
    if (!crop || !canvasRef.current || !imgRef.current) {
      return;
    }

    const img = imgRef.current;
    const canvas = canvasRef.current;

    const scaleX = img.naturalWidth / img.width;
    const scaleY = img.naturalHeight / img.height;
    const ctx = canvas.getContext('2d');
    const pixelRatio = window.devicePixelRatio;

    canvas.width = crop.width * pixelRatio;
    canvas.height = crop.height * pixelRatio;

    if (ctx) {
      ctx.setTransform(pixelRatio, 0, 0, pixelRatio, 0, 0);
      ctx.imageSmootingEnabled = false;

      ctx.drawImage(
        img,
        crop.x * scaleX,
        crop.y * scaleY,
        crop.width * scaleX,
        crop.height * scaleY,
        0,
        0,
        crop.width,
        crop.height
      );
    }
  }, [crop]);

  if (!cropImg) {
    return <Spinner />;
  }

  return (
    <Modal open>
      <Modal.Header>
        {header}
      </Modal.Header>
      <Modal.Content>
        <div>{text}</div>
        <div className={styles.content}>
          <div>
            <ReactCrop
              src={cropImg}
              crop={currCrop}
              onChange={onCropChange}
              onImageLoaded={imageLoad}
              onComplete={setCrop}
              imageStyle={{ maxWidth: '100%' }}
            />
          </div>
          <div>
            {crop ? (
              <canvas
                ref={canvasRef}
                style={{
                  maxWidth: '100%'
                }}
              />
            ) : null}
          </div>
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button floated="right" onClick={onCancel}>Cancel</Button>
        <Button floated="right" color="teal" onClick={exportImage}>Save</Button>
        <div style={{ clear: 'both' }} />
      </Modal.Actions>
    </Modal>
  );
};

ImageCropModal.propTypes = {
  image: PropTypes.objectOf(PropTypes.any).isRequired,
  onFinish: PropTypes.func.isRequired,
  onCancel: PropTypes.func.isRequired,
  header: PropTypes.string.isRequired,
  text: PropTypes.string.isRequired
};

export default ImageCropModal;
