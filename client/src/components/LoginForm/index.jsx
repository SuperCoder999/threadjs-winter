import React, { useState } from 'react';
import PropTypes from 'prop-types';
import validator from 'validator';
import { Form, Button, Segment, Popup } from 'semantic-ui-react';
import { NotificationManager } from 'react-notifications';
import ResetTokenSent from '../ResetTokenSent';
import styles from './styles.module.scss';

const LoginForm = ({ login }) => {
  const [showResetModal, setShowResetPassword] = useState(false);
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [isLoading, setIsLoading] = useState(false);
  const [isEmailValid, setIsEmailValid] = useState(true);
  const [isPasswordValid, setIsPasswordValid] = useState(true);

  const toggleShowResetPassword = () => {
    if (email && isEmailValid) {
      setShowResetPassword(true);
    }
  };

  const emailChanged = data => {
    setEmail(data);
    setIsEmailValid(true);
  };

  const passwordChanged = data => {
    setPassword(data);
    setIsPasswordValid(true);
  };

  const handleLoginClick = async () => {
    const isValid = isEmailValid && isPasswordValid;

    if (!isValid || isLoading) {
      return;
    }

    setIsLoading(true);

    try {
      await login({ email, password });
    } catch (err) {
      NotificationManager.error(err.message, 'Error');
      setIsLoading(false);
    }
  };

  return (
    <Form name="loginForm" size="large" onSubmit={handleLoginClick}>
      <Segment>
        <Form.Input
          fluid
          icon="at"
          iconPosition="left"
          placeholder="Email"
          type="email"
          error={!isEmailValid}
          onChange={ev => emailChanged(ev.target.value)}
          onBlur={() => setIsEmailValid(validator.isEmail(email))}
        />
        <Form.Input
          fluid
          icon="lock"
          iconPosition="left"
          placeholder="Password"
          type="password"
          error={!isPasswordValid}
          onChange={ev => passwordChanged(ev.target.value)}
          onBlur={() => setIsPasswordValid(Boolean(password))}
        />
        <Popup
          content="Enter email to reset password"
          disabled={Boolean(email && isEmailValid)}
          trigger={(
            <div
              onClick={toggleShowResetPassword}
              className={styles.anchorButton}
            >
              Forgot password?
            </div>
          )}
        />
        <Button type="submit" color="teal" fluid size="large" loading={isLoading} primary>
          Login
        </Button>
        {showResetModal
          ? <ResetTokenSent email={email} onClose={() => setShowResetPassword(false)} />
          : null}
      </Segment>
    </Form>
  );
};

LoginForm.propTypes = {
  login: PropTypes.func.isRequired
};

export default LoginForm;
