import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'semantic-ui-react';
import * as service from '../../services/authService';

const ResetTokenSent = ({ onClose, email }) => {
  const [showResent, setShowResent] = useState(false);

  const sendLink = async (shouldResend = false) => {
    await service.generatePasswordResetToken(email);

    if (shouldResend) {
      setShowResent(true);
      setTimeout(setShowResent, 1000, false);
    }
  };

  useEffect(() => {
    sendLink();
    // It should be called only ONE time.
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Modal open size="small" onClose={onClose}>
      <Modal.Header>
        Reset password link sent
      </Modal.Header>
      <Modal.Content>
        Reset password link was sent to your email
        <div style={{ color: 'green', display: showResent ? 'block' : 'none' }}>
          Link sent second time
        </div>
      </Modal.Content>
      <Modal.Actions>
        <Button onClick={() => sendLink(true)}>
          Send link second time
        </Button>
        <Button primary onClick={onClose}>
          OK
        </Button>
      </Modal.Actions>
    </Modal>
  );
};

ResetTokenSent.propTypes = {
  onClose: PropTypes.func.isRequired,
  email: PropTypes.string.isRequired
};

export default ResetTokenSent;
