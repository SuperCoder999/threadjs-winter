import React, { useState } from 'react';
import PropTypes from 'prop-types';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { Card, Image, Label, Icon, Popup } from 'semantic-ui-react';
import * as imageService from 'src/services/imageService';
import UpdatePost from '../UpdatePost';
import { updatePost, deletePost } from '../../containers/Thread/actions';

import styles from './styles.module.scss';

const Post = ({
  post,
  likePost,
  dislikePost,
  toggleExpandedPost,
  sharePost,
  updatePost: changePost,
  deletePost: removePost,
  authorizedUser
}) => {
  const [showUpdate, setShowUpdate] = useState(false);

  const {
    id,
    image,
    body,
    user,
    likeCount,
    dislikeCount,
    commentCount,
    postReactions,
    createdAt
  } = post;

  const date = moment(createdAt).fromNow();
  const uploadImage = file => imageService.uploadImage(file);

  const maxShowingLikes = 15;
  const likeObjects = postReactions.filter(react => react.isLike);
  const showingLikes = likeObjects.slice(0, maxShowingLikes);
  const moreLikes = likeObjects.length - showingLikes.length;
  const showingLikesText = showingLikes.map(like => like.user.username).join(', ');
  const moreLikesText = moreLikes ? ` and ${moreLikes} others` : '';

  return (
    <Card style={{ width: '100%' }}>
      {image && <Image src={image.link} wrapped ui={false} />}
      <Card.Content>
        <Card.Meta>
          <span className="date">
            posted by
            {' '}
            {user.username}
            {' - '}
            {date}
          </span>
        </Card.Meta>
        <Card.Description>
          {body}
        </Card.Description>
      </Card.Content>
      <Card.Content extra>
        <Popup
          content={`${showingLikesText}${moreLikesText} like this`}
          disabled={!showingLikes.length}
          trigger={(
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => likePost(id)}>
              <Icon name="thumbs up" />
              {likeCount}
            </Label>
          )}
        />
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => dislikePost(id)}>
          <Icon name="thumbs down" />
          {dislikeCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => toggleExpandedPost(id)}>
          <Icon name="comment" />
          {commentCount}
        </Label>
        <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => sharePost(id)}>
          <Icon name="share alternate" />
        </Label>
        {user.id === authorizedUser.id ? (
          <>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => setShowUpdate(true)}>
              <Icon name="edit" />
            </Label>
            <Label basic size="small" as="a" className={styles.toolbarBtn} onClick={() => removePost(id)}>
              <Icon name="trash" />
            </Label>
            <UpdatePost
              shown={showUpdate}
              postId={id}
              defaultBody={body}
              defaultImage={image}
              updatePost={changePost}
              uploadImage={uploadImage}
              onSubmit={() => setShowUpdate(false)}
            />
          </>
        ) : null}
      </Card.Content>
    </Card>
  );
};

Post.propTypes = {
  post: PropTypes.objectOf(PropTypes.any).isRequired,
  likePost: PropTypes.func.isRequired,
  dislikePost: PropTypes.func.isRequired,
  toggleExpandedPost: PropTypes.func.isRequired,
  sharePost: PropTypes.func.isRequired,
  updatePost: PropTypes.func.isRequired,
  deletePost: PropTypes.func.isRequired,
  authorizedUser: PropTypes.objectOf(PropTypes.any).isRequired
};

const actions = { updatePost, deletePost };

const mapStateToProps = rootState => ({
  authorizedUser: rootState.profile.user
});

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Post);
