import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { NavLink } from 'react-router-dom';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import { Header as HeaderUI, Image, Grid, Icon, Button } from 'semantic-ui-react';
import { updateProfile } from '../../containers/Profile/actions';

import styles from './styles.module.scss';

const Header = ({ user, logout, updateProfile: updateUser }) => {
  const [status, setStatus] = useState(user.status);

  return (
    <div className={styles.headerWrp}>
      <Grid centered container columns="2">
        <Grid.Column>
          {user && (
            <NavLink exact to="/">
              <div className={styles.left}>
                <Image circular src={getUserImgLink(user.image)} className={styles.image} />
                <div>
                  <HeaderUI className={styles.header}>{user.username}</HeaderUI>
                  <div className="ui left icon transparent input">
                    <input
                      type="text"
                      className={styles.status}
                      placeholder="Status"
                      value={status || ''}
                      onChange={event => setStatus(event.target.value)}
                      onBlur={() => updateUser({ status })}
                    />
                    <Icon name="edit" style={{ fontSize: 12 }} />
                  </div>
                </div>
              </div>
            </NavLink>
          )}
        </Grid.Column>
        <Grid.Column textAlign="right">
          <NavLink exact activeClassName="active" to="/profile" className={styles.menuBtn}>
            <Icon name="user circle" size="large" />
          </NavLink>
          <Button basic icon type="button" className={`${styles.menuBtn} ${styles.logoutBtn}`} onClick={logout}>
            <Icon name="log out" size="large" />
          </Button>
        </Grid.Column>
      </Grid>
    </div>
  );
};

Header.propTypes = {
  logout: PropTypes.func.isRequired,
  user: PropTypes.objectOf(PropTypes.any).isRequired,
  updateProfile: PropTypes.func.isRequired
};

const actions = { updateProfile };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(() => ({}), mapDispatchToProps)(Header);
