import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { Comment as CommentUI, Icon, Popup } from 'semantic-ui-react';
import moment from 'moment';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import { getUserImgLink } from 'src/helpers/imageHelper';
import UpdateComment from '../UpdateComment';
import * as service from '../../services/commentService';
import { toggleExpandedPost } from '../../containers/Thread/actions';

import styles from './styles.module.scss';

const Comment = ({ comment: defaultComment, toggleExpandedPost: reloadPost }) => {
  const [comment, setComment] = useState(defaultComment);
  const [updateShown, setUpdateShown] = useState(false);

  const {
    id: commentId,
    body,
    createdAt,
    user,
    likeCount,
    dislikeCount,
    postId,
    commentReactions
  } = comment;

  const like = async () => {
    const { id, updated } = await service.reactComment(commentId);
    const diff = id ? 1 : -1;
    const otherDiff = updated ? -1 : 0;

    const newLikeCount = Number(likeCount) + diff;
    const newDislikeCount = Number(dislikeCount) + otherDiff;

    setComment({
      ...comment,
      likeCount: newLikeCount,
      dislikeCount: newDislikeCount
    });
  };

  const dislike = async () => {
    const { id, updated } = await service.reactComment(commentId, false);
    const diff = id ? 1 : -1;
    const otherDiff = updated ? -1 : 0;

    const newDislikeCount = Number(dislikeCount) + diff;
    const newLikeCount = Number(likeCount) + otherDiff;

    setComment({
      ...comment,
      likeCount: newLikeCount,
      dislikeCount: newDislikeCount
    });
  };

  const update = async data => {
    const result = await service.updateComment(commentId, data);

    setComment({
      ...comment,
      ...result
    });
  };

  const deleteComment = async () => {
    await service.deleteComment(commentId);
    reloadPost(postId);
  };

  const maxShowingLikes = 10;
  const likeObjects = commentReactions.filter(react => react.isLike);
  const showingLikes = likeObjects.slice(0, maxShowingLikes);
  const moreLikes = likeObjects.length - showingLikes.length;
  const showingLikesText = showingLikes.map(suspectedLike => suspectedLike.user.username).join(', ');
  const moreLikesText = moreLikes ? ` and ${moreLikes} others` : '';

  return (
    <CommentUI className={styles.comment}>
      <CommentUI.Avatar src={getUserImgLink(user.image)} />
      <CommentUI.Content>
        <CommentUI.Author as="a">
          {user.username}
        </CommentUI.Author>
        <CommentUI.Metadata>
          {moment(createdAt).fromNow()}
        </CommentUI.Metadata>
        {user.status ? (
          <>
            <br />
            <CommentUI.Metadata>
              {user.status}
            </CommentUI.Metadata>
          </>
        ) : null}
        <CommentUI.Text>
          {body}
        </CommentUI.Text>
        <CommentUI.Actions>
          <Popup
            content={`${showingLikesText}${moreLikesText} like this`}
            disabled={!showingLikes.length}
            trigger={(
              <CommentUI.Action onClick={like}>
                <Icon name="thumbs up" />
                {likeCount}
              </CommentUI.Action>
            )}
          />
          <CommentUI.Action onClick={dislike}>
            <Icon name="thumbs down" />
            {dislikeCount}
          </CommentUI.Action>
          <CommentUI.Action onClick={() => setUpdateShown(true)}>
            <Icon name="edit" />
          </CommentUI.Action>
          <CommentUI.Action onClick={deleteComment}>
            <Icon name="trash" />
          </CommentUI.Action>
        </CommentUI.Actions>
        <UpdateComment
          shown={updateShown}
          defaultBody={body}
          updateComment={update}
          onSubmit={() => setUpdateShown(false)}
        />
      </CommentUI.Content>
    </CommentUI>
  );
};

Comment.propTypes = {
  comment: PropTypes.objectOf(PropTypes.any).isRequired,
  toggleExpandedPost: PropTypes.func.isRequired
};

const actions = { toggleExpandedPost };

const mapDispatchToProps = dispatch => bindActionCreators(actions, dispatch);

export default connect(() => ({}), mapDispatchToProps)(Comment);
