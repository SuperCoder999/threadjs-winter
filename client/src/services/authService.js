import callWebApi from 'src/helpers/webApiHelper';

export const login = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/login',
    type: 'POST',
    request
  });
  return response.json();
};

export const registration = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/register',
    type: 'POST',
    request
  });
  return response.json();
};

export const getCurrentUser = async () => {
  try {
    const response = await callWebApi({
      endpoint: '/api/auth/user',
      type: 'GET'
    });
    return response.json();
  } catch (e) {
    return null;
  }
};

export const updateUser = async request => {
  const response = await callWebApi({
    endpoint: '/api/auth/update',
    type: 'PUT',
    request
  });

  return response.json();
};

export const generatePasswordResetToken = async email => {
  const response = await callWebApi({
    endpoint: '/api/auth/gen_reset_password',
    type: 'POST',
    request: {
      email
    }
  });

  return response.json();
};

export const resetPassword = async (token, password) => {
  const response = await callWebApi({
    endpoint: `/api/auth/reset_password/${token}`,
    type: 'POST',
    request: {
      password
    }
  });

  return response.json();
};

export const getByResetToken = async token => {
  const response = await callWebApi({
    endpoint: `/api/auth/token/${token}`,
    type: 'GET'
  });

  return response.json();
};
