import callWebApi from 'src/helpers/webApiHelper';

export const getAllPosts = async filter => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'GET',
    query: filter
  });
  return response.json();
};

export const addPost = async request => {
  const response = await callWebApi({
    endpoint: '/api/posts',
    type: 'POST',
    request
  });
  return response.json();
};

export const updatePost = async (postId, request) => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}`,
    type: 'PUT',
    request
  });

  return response.json();
};

export const deletePost = async postId => {
  const response = await callWebApi({
    endpoint: `/api/posts/${postId}`,
    type: 'DELETE'
  });

  return response.json();
};

export const getPost = async id => {
  const response = await callWebApi({
    endpoint: `/api/posts/${id}`,
    type: 'GET'
  });
  return response.json();
};

export const reactPost = async (postId, isLike = true) => {
  const response = await callWebApi({
    endpoint: '/api/posts/react',
    type: 'PUT',
    request: {
      postId,
      isLike
    }
  });
  return response.json();
};

export const sharePost = async (email, url) => {
  const response = await callWebApi({
    endpoint: '/api/posts/share',
    type: 'POST',
    request: {
      email,
      url
    }
  });

  return response.json();
};

// should be replaced by approppriate function
export const getPostByHash = async hash => getPost(hash);
